export enum AttributeQueue {
  Create = 'Create new news',
  Update = 'Update news',
  Delete = 'Delete news',
}

export enum Email {
  Source = 'sergienya.olegka@gmail.com',
  ToAddresses = 'sergienya.oleg@yandex.ru',
}
