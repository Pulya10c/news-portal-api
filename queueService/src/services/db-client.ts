import * as AWS from 'aws-sdk';
import { INews } from '../models';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

export const updateItemDynamoDB = async ({
  id,
  author,
  title,
  description,
  content,
  publishedAt,
}: INews): Promise<unknown> => {
  const params = {
    TableName: 'news',
    Key: {
      id,
    },
    UpdateExpression: 'set author=:r, title=:p, description=:a, content=:k, publishedAt=:s',
    ExpressionAttributeValues: {
      ':r': author,
      ':p': title,
      ':a': description,
      ':k': content,
      ':s': publishedAt,
    },
    ReturnValues: 'ALL_NEW',
  };

  return dynamoDB
    .update(params)
    .promise()
    .then((res) => res.Attributes)
    .catch((err) => err);
};

export const createItemDynamoDB = async (news: INews): Promise<unknown> => {
  const params = {
    TableName: 'news',
    Item: {
      ...news,
    },
    ReturnValues: 'NONE',
  };

  return dynamoDB
    .put(params)
    .promise()
    .then(() => ({
      status: 'ok',
      error: null,
    }))
    .catch((error) => ({
      error,
      status: 'error',
    }));
};

export const deleteItemDynamoDB = async (id: string): Promise<unknown> => {
  const params = {
    TableName: 'news',
    Key: {
      id,
    },
    ReturnValues: 'NONE',
  };

  return dynamoDB
    .delete(params)
    .promise()
    .then(() => ({
      status: 'ok',
      error: null,
    }))
    .catch((error) => ({
      error,
      status: 'error',
    }));
};
