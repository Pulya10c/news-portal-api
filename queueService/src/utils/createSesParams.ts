import { Email } from '../models/constants';
import { SendEmailRequest } from 'aws-sdk/clients/ses';

export const createSesParams = (attribute: string): SendEmailRequest => {
  const htmlBody = `
      <!DOCTYPE html>
      <html>
        <head></head>
        <body>
          <h1>News Portal</h1>
          <h3>You have sent a request for action with the news!</h3>
          <p>Your request for<h2 style="color: blue;">${' ' + attribute + ' '}</h2>has been completed successfully</p>
          <h4>You can see the changes on the website: <a href="https://d1s7i3s5pr2i21.cloudfront.net">News portal</a></h4>
        </body>
      </html>
    `;

  const sesParams: SendEmailRequest = {
    Destination: {
      ToAddresses: [Email.ToAddresses],
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: htmlBody,
        },
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'News portal admin',
      },
    },
    Source: Email.Source,
  };

  return sesParams;
};
