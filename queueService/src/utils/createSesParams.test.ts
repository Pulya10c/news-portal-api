import { createSesParams } from './createSesParams';

describe('Component "createResponse"', () => {
  it('should function return status 500', () => {
    const body = `<!DOCTYPE html>
    <html>
      <head></head>
      <body>
        <h1>News Portal</h1>
        <h3>You have sent a request for action with the news!</h3>
        <p>Your request for<h2 style="color: blue;"> test </h2>has been completed successfully</p>
        <h4>You can see the changes on the website: <a href="https://d1s7i3s5pr2i21.cloudfront.net">News portal</a></h4>
      </body>
    </html>
  `;
    const params = createSesParams('test');
    expect(params.Message.Body.Html.Data.replace(/\s/g, '')).toBe(body.replace(/\s/g, ''));
  });
});
