import { SQSHandler, SQSMessageAttributes } from 'aws-lambda';
import * as AWS from 'aws-sdk';
import 'source-map-support/register';
import { AttributeQueue } from '../models/constants';
import { INews } from '../models';
import { DBClient } from '../services';
import { createSesParams } from '../utils/createSesParams';

const ses = new AWS.SES({ apiVersion: '2010-12-01' });

export const receiverQueue: SQSHandler = async (event) => {
  console.info('Lambda called with the event: ', JSON.stringify(event));
  let response;
  let result;
  try {
    for (const record of event.Records) {
      const { AttributeNameHere }: SQSMessageAttributes = record.messageAttributes;
      const sesParams = createSesParams(AttributeNameHere.stringValue);

      switch (AttributeNameHere.stringValue) {
        case AttributeQueue.Create:
          const newNews = JSON.parse(record.body) as INews;
          result = await DBClient.createItemDynamoDB(newNews);
          response = await ses.sendEmail(sesParams).promise();
          break;
        case AttributeQueue.Update:
          const updateNews = JSON.parse(record.body) as INews;
          result = await DBClient.updateItemDynamoDB(updateNews);
          response = await ses.sendEmail(sesParams).promise();
          break;
        case AttributeQueue.Delete:
          const { id } = JSON.parse(record.body);
          result = await DBClient.deleteItemDynamoDB(id);
          response = await ses.sendEmail(sesParams).promise();
          break;
        default:
          console.error('Invalid attribute: ', AttributeNameHere.stringValue);
      }
    }
    console.log('The result of writing data to DynamoDB: ', JSON.stringify(result));
    console.log('SES service response: ', JSON.stringify(response));
  } catch (error) {
    console.error(JSON.stringify(error));
  }
};
