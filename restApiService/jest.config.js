const { defaults } = require('jest-config');
const path = require('path');
const nodeModulesDirPath = path.resolve(__dirname, './node_modules/');

module.exports = {
  verbose: true,
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts'],
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts)$'],
  testRegex: '((\\.|/*.)(test))\\.ts?$',
  transform: {
    '^.+\\.(ts)$': `${nodeModulesDirPath}/babel-jest`,
  },
};
