import { APIGatewayProxyHandler } from 'aws-lambda';
import 'source-map-support/register';
import { createResponse } from '../utils/createResponse';
import { DBClient } from '../services';
import { INews } from '../models';

export const getOwnNews: APIGatewayProxyHandler = async () => {
  try {
    const newsDB = await DBClient.getItemsAllDynamoDB();

    return createResponse({ status: 'Ok', news: [...newsDB] as INews[] });
  } catch (error) {
    return createResponse({ error, news: [] });
  }
};
