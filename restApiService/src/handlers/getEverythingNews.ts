import { APIGatewayProxyHandler } from 'aws-lambda';
import fetch from 'node-fetch';
import 'source-map-support/register';
import { createResponse } from '../utils/createResponse';
import { DBClient } from '../services';

export const getEverythingNews: APIGatewayProxyHandler = async (event) => {
  try {
    const { query } = event.queryStringParameters;
    const url = `https://newsapi.org/v2/everything?q=${query}&apiKey=b514e755bee248ed8992d4034d670433`;
    const result = await fetch(url, { method: 'GET' });
    const { articles, status, ...error } = await result.json();

    const response = await DBClient.queryDynamoDB(query);

    const currentError =
      status === 'ok' ? undefined : error || response.status === 'ok' ? undefined : { message: response.error };
    const currentStatus = status === 'ok' ? 'ok' : undefined || response.status === 'ok' ? 'ok' : undefined;

    return createResponse({
      status: currentStatus || 'error',
      news: [...articles, ...response.newsList],
      error: currentError,
      event: response,
    });
  } catch (error) {
    return createResponse({ error, news: [] });
  }
};
