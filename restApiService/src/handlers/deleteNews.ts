import { APIGatewayProxyHandler } from 'aws-lambda';
import { SQS } from 'aws-sdk';
import 'source-map-support/register';
import { createResponse } from '../utils/createResponse';
import { createResponseNoBody } from '../utils/createResponseNoBody';
import { AttributeQueue, MessageQueue } from '../models/constants';
import { getQueueUrl } from '../utils/getQueueUrl';

const sqs = new SQS();

export const deleteNews: APIGatewayProxyHandler = async (event, context) => {
  if (!event.pathParameters) {
    return createResponseNoBody({});
  }

  try {
    const { id } = event.pathParameters;

    const queueUrl: string = getQueueUrl(context);

    const event1 = await sqs
      .sendMessage({
        QueueUrl: queueUrl,
        MessageBody: JSON.stringify({ id }),
        MessageAttributes: {
          AttributeNameHere: {
            StringValue: AttributeQueue.Delete,
            DataType: 'String',
          },
        },
      })
      .promise();

    return createResponse({
      id: '',
      event: event1,
      error: { message: MessageQueue.Placed, name: AttributeQueue.Delete },
      status: 'ok',
      news: [],
    });
  } catch (error) {
    return createResponse({ id: '', error, status: 'error', news: [] });
  }
};
