import { APIGatewayProxyHandler } from 'aws-lambda';
import fetch from 'node-fetch';
import 'source-map-support/register';
import { createResponse } from '../utils/createResponse';
import { DBClient } from '../services';

export const getTopNews: APIGatewayProxyHandler = async () => {
  try {
    const newsDB = await DBClient.getItemsAllDynamoDB();
    const url = 'https://newsapi.org/v2/top-headlines?country=us&apiKey=b514e755bee248ed8992d4034d670433';
    const result = await fetch(url, { method: 'GET' });
    const { articles, status, ...error } = await result.json();

    return createResponse({ error, status, news: [...articles, ...newsDB] });
  } catch (error) {
    return createResponse({ error, news: [] });
  }
};
