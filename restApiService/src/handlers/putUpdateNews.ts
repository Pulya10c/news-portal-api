import { APIGatewayProxyHandler } from 'aws-lambda';
import { SQS } from 'aws-sdk';
import 'source-map-support/register';
import { createResponse } from '../utils/createResponse';
import { INews } from '../models';
import { createResponseNoBody } from '../utils/createResponseNoBody';
import { getQueueUrl } from '../utils/getQueueUrl';
import { MessageQueue, AttributeQueue } from '../models/constants';

const sqs = new SQS();

export const putUpdateNews: APIGatewayProxyHandler = async (event, context) => {
  if (!event.body) {
    return createResponseNoBody({});
  }

  try {
    const queueUrl: string = getQueueUrl(context);

    const news = JSON.parse(event.body) as INews;

    const event1 = await sqs
      .sendMessage({
        QueueUrl: queueUrl,
        MessageBody: JSON.stringify(news),
        MessageAttributes: {
          AttributeNameHere: {
            StringValue: AttributeQueue.Update,
            DataType: 'String',
          },
        },
      })
      .promise();

    return createResponse({
      event: event1,
      error: { message: MessageQueue.Placed, name: AttributeQueue.Update },
      status: 'ok',
      news: [],
    });
  } catch (error) {
    return createResponse({ event, error, status: 'error', news: [] });
  }
};
