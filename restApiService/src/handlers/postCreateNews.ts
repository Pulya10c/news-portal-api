import { APIGatewayProxyHandler } from 'aws-lambda';
import { SQS } from 'aws-sdk';
import 'source-map-support/register';
import { createResponse } from '../utils/createResponse';
import { INews } from '../models';
import { v4 as uuidv4 } from 'uuid';
import { MessageQueue, AttributeQueue } from '../models/constants';
import { getQueueUrl } from '../utils/getQueueUrl';
import { createResponseNoBody } from '../utils/createResponseNoBody';

const sqs = new SQS();

export const postCreateNews: APIGatewayProxyHandler = async (event, context) => {
  const rest = {
    id: uuidv4(),
    url: undefined,
    urlToImage: undefined,
    publishedAt: new Date().getTime(),
  };

  if (!event.body) {
    return createResponseNoBody({});
  }

  try {
    const queueUrl: string = getQueueUrl(context);

    const news = JSON.parse(event.body) as INews;
    const newNews = { ...news, ...rest };

    const event1 = await sqs
      .sendMessage({
        QueueUrl: queueUrl,
        MessageBody: JSON.stringify(newNews),
        MessageAttributes: {
          AttributeNameHere: {
            StringValue: AttributeQueue.Create,
            DataType: 'String',
          },
        },
      })
      .promise();

    return createResponse({
      event: event1,
      error: { message: MessageQueue.Placed, name: AttributeQueue.Create },
      status: 'ok',
      news: [],
    });
  } catch (error) {
    return createResponse({ error, status: 'error', news: [] });
  }
};
