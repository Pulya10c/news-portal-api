import { ICreateResponse } from '../models';
import { v4 as uuidv4 } from 'uuid';

export const createResponse: ICreateResponse = ({ status, news, error, event, id }) => {
  const result =
    status.toLocaleLowerCase() !== 'ok'
      ? { status, ...error, articles: [] }
      : {
          status,
          id,
          articles: news.length
            ? news.map((el) => (el?.id ? { ...el, db: true } : { ...el, db: false, id: uuidv4() }))
            : [],
        };
  return {
    statusCode: status.toLocaleLowerCase() !== 'ok' ? 500 : 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    body: JSON.stringify(
      {
        result,
        event,
      },
      null,
      2,
    ),
  };
};
