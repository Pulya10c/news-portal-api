import { createResponse } from './createResponse';

describe('Component "createResponse"', () => {
  it('should function return status 500', () => {
    const res = createResponse({
      status: 'error',
      news: [],
    });
    expect(res.statusCode).toBe(500);
  });
});
