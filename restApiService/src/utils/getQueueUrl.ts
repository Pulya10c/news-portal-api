import { Context } from 'aws-lambda/handler';

export const getQueueUrl = (context: Context): string => {
  const region = context.invokedFunctionArn.split(':')[3];
  const accountId = context.invokedFunctionArn.split(':')[4];
  const queueUrl = `https://sqs.${region}.amazonaws.com/${accountId}/NewsUpdateQueue`;
  return queueUrl;
};
