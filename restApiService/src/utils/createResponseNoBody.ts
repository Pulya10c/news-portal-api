import { createResponse } from './createResponse';
import { MessageQueue } from '../models/constants';
import { ICreateResponse } from '../models';

export const createResponseNoBody: ICreateResponse = () => {
  return createResponse({
    error: {
      message: MessageQueue.NoBody,
      name: 'error',
    },
    status: 'error',
    news: [],
  });
};
