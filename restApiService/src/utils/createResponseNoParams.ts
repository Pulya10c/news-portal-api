import { createResponse } from './createResponse';
import { MessageQueue } from '../models/constants';
import { ICreateResponse } from '../models';

export const createResponseNoParams: ICreateResponse = () => {
  return createResponse({
    error: {
      message: MessageQueue.NoParams,
      name: 'error',
    },
    status: 'error',
    news: [],
  });
};
