import * as AWS from 'aws-sdk';

interface IDBQueryResponse {
  status: string;
  error: string | null;
  newsList: AWS.DynamoDB.DocumentClient.ItemList;
}

const dynamoDB = new AWS.DynamoDB.DocumentClient();

export const getItemsAllDynamoDB = async (): Promise<AWS.DynamoDB.DocumentClient.ItemList> => {
  const params = {
    TableName: 'news',
  };

  return dynamoDB
    .scan(params)
    .promise()
    .then((res) => res.Items)
    .catch((err) => err);
};

export const queryDynamoDB = async (query: string): Promise<IDBQueryResponse> => {
  const params = {
    TableName: 'news',
    ProjectionExpression: 'id, title, author, description, content, publishedAt',
    FilterExpression: 'contains(#tl, :word) OR contains(#dsc, :word) OR contains(#cnt, :word)',
    ExpressionAttributeNames: {
      '#tl': 'title',
      '#dsc': 'description',
      '#cnt': 'content',
    },
    ExpressionAttributeValues: {
      ':word': query,
    },
  };

  return dynamoDB
    .scan(params)
    .promise()
    .then(({ Items }) => ({
      status: 'ok',
      error: null,
      newsList: Items,
    }))
    .catch((error) => ({
      error,
      status: 'error',
      newsList: [],
    }));
};
