export enum AttributeQueue {
  Create = 'Create new news',
  Update = 'Update news',
  Delete = 'Delete news',
}

export enum MessageQueue {
  NoBody = 'No body was found',
  Placed = 'Message placed in the Queue!',
  NoParams = 'No params (id) was found',
}
