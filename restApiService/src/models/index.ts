import { INews } from './news';
import { IErrorResponse } from './errorResponse';
import { ICreateResponse, IResponse } from './response';

export { INews, IErrorResponse, ICreateResponse, IResponse };
