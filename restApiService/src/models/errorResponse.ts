export interface IErrorResponse {
  status?: string;
  code?: string;
  message?: string;
}
