import { INews } from './news';
import { APIGatewayProxyResult } from 'aws-lambda';

export interface IResponse {
  statusCode?: number;
  headers?: {
    [key: string]: string;
  };
  body?: string;
}

interface ICreateResponseArg {
  status?: string;
  news?: INews[];
  error?: Error | { [key: string]: string } | undefined;
  event?: unknown;
  id?: string;
}

export type ICreateResponse = (arg: ICreateResponseArg) => APIGatewayProxyResult;
