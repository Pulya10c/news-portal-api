import { Email } from '../models/constants';
import { SendEmailRequest } from 'aws-sdk/clients/ses';

export const createSesParams = (): SendEmailRequest => {
  const htmlBody = `
      <!DOCTYPE html>
      <html>
        <head></head>
        <body>
          <h1>News Portal</h1>
          <p style="color: red;">
          Unexpected errors occurred while working with news. You can see additional information in the database</p>
        </body>
      </html>
    `;

  const sesParams: SendEmailRequest = {
    Destination: {
      ToAddresses: [Email.ToAddresses],
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: htmlBody,
        },
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'NEWS PORTAL ERRORS',
      },
    },
    Source: Email.Source,
  };

  return sesParams;
};
