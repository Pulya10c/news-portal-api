import { createSesParams } from './createSesParams';

describe('Component "createResponse"', () => {
  it('should function return status 500', () => {
    const body = `
    <!DOCTYPE html>
    <html>
      <head></head>
      <body>
        <h1>News Portal</h1>
        <p style="color: red;">
        Unexpected errors occurred while working with news. You can see additional information in the database</p>
      </body>
    </html>
  `;
    const params = createSesParams();
    expect(params.Message.Body.Html.Data.replace(/\s/g, '')).toBe(body.replace(/\s/g, ''));
  });
});
