import * as AWS from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

export const createItemDynamoDB = async (errorMessage: string): Promise<unknown> => {
  const params = {
    TableName: 'news',
    Item: {
      id: uuidv4(),
      errorMessage,
    },
    ReturnValues: 'NONE',
  };

  return dynamoDB
    .put(params)
    .promise()
    .then(() => ({
      status: 'ok',
      error: null,
    }))
    .catch((error) => ({
      error,
      status: 'error',
    }));
};
