import { SQSHandler, SQSMessageAttributes } from 'aws-lambda';
import * as AWS from 'aws-sdk';
import 'source-map-support/register';
import { DBClient } from '../services';
import { createSesParams } from '../utils/createSesParams';

const ses = new AWS.SES({ apiVersion: '2010-12-01' });

export const receiverDLQueue: SQSHandler = async (event) => {
  console.info('Lambda called with the event: ', JSON.stringify(event));
  let response;
  let result;
  try {
    for (const record of event.Records) {
      const sesParams = createSesParams();
      const errorMessage = JSON.stringify(record);
      result = await DBClient.createItemDynamoDB(errorMessage);
      response = await ses.sendEmail(sesParams).promise();

      console.log('The error messages of writing data to DynamoDB: ', JSON.stringify(result));
      console.log('SES response: ', JSON.stringify(response));
    }
  } catch (error) {
    console.error(JSON.stringify(error));
  }
};
